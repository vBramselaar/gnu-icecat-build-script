#!/bin/bash

readonly GNU_GIT="git.savannah.gnu.org/cgit/gnuzilla.git/snapshot"
readonly COMMIT_SHA="08202dd51b8c05e17238549e7922b1e02f4a0d1a"

function extract_var_from_file()
{
	local VAR="$1"
	local FILE="$2"
	grep -oP "(?<=^readonly $VAR=)[0-9]+" "$FILE"
}

function download ()
{
	set -e
	rm -f "gnuzilla-${COMMIT_SHA}.tar.gz"
	wget --no-verbose "https://${GNU_GIT}/gnuzilla-${COMMIT_SHA}.tar.gz"
	tar xf "gnuzilla-${COMMIT_SHA}.tar.gz"

	rm "gnuzilla-${COMMIT_SHA}.tar.gz"

	echo "Downloaded GNU Icecat project"
	set +e
}

function patch_icecat ()
{
	set -e

	cd "gnuzilla-${COMMIT_SHA}"

	# Forgot the reason for this :(
	sed -e 's;find l10n -wholename '\''\*/brand.dtd'\'' | xargs;find l10n -wholename '\''\*/brand.dtd'\'' | xargs -r;g' -i makeicecat

	# "gpg: keyserver receive failed: Connection timed out" error in build server
	sed -i 's/keyserver.ubuntu.com/hkps:\/\/keyserver.ubuntu.com/g' makeicecat

	# Remove tarball creation, will do it myself
	sed -i 's/^finalize_sourceball$//g' makeicecat

	cd ..
	echo "Patched GNU Icecat project"
	set +e
}

function generate_source ()
{
	echo ""
	echo "#############################################"
	echo "Going to build commit: ${COMMIT_SHA}"
	echo "Version: ${FFVERSION}"
	echo "#############################################"

	cd "gnuzilla-${COMMIT_SHA}"
	./makeicecat
	RETURN=$?
	cd ..
	if [ $RETURN -ne 0 ]; then
		return $RETURN
	fi

	echo "Inside gnuzilla-${COMMIT_SHA}: "
	ls -a "./gnuzilla-${COMMIT_SHA}"

	echo "Inside gnuzilla-${COMMIT_SHA}/output: "
	ls -a "./gnuzilla-${COMMIT_SHA}/output"

	tar cfj "icecat-${FFVERSION}.tar.bz2" "gnuzilla-${COMMIT_SHA}/output/icecat-${FFVERSION}"
	mv "icecat-${FFVERSION}.tar.bz2" "./result/"

	echo "Build GNU Icecat project"
}

download

FFMAJOR=$(extract_var_from_file "FFMAJOR" "gnuzilla-${COMMIT_SHA}/makeicecat")
FFMINOR=$(extract_var_from_file "FFMINOR" "gnuzilla-${COMMIT_SHA}/makeicecat")
FFSUB=$(extract_var_from_file "FFSUB" "gnuzilla-${COMMIT_SHA}/makeicecat")

readonly FFVERSION=${FFMAJOR}.${FFMINOR}.${FFSUB}

patch_icecat
for i in {1..10}; do
	if generate_source; then
		exit 0
	fi
	echo "Retrying generating GNU Icecat source code..."
done
exit 1
