# GNU IceCat build-script
[![Build Status](https://api.cirrus-ci.com/github/vbramselaar/icecat-build-script-mirror.svg)](https://cirrus-ci.com/github/vbramselaar/icecat-build-script-mirror)

<img src="https://gitlab.com/uploads/-/system/project/avatar/27571614/icecat.png"  width="200">

Build script to build icecat from the gnuzilla repository. This build is focused on working for the latest Debian stable release.

## Building
### Docker
To build with docker, first build the docker image by running this command in the root of the repository:
``` bash
docker build . -t vbramselaar/icecat-builder
```
Then to start generating the GNU Icecat source code:
``` bash
mkdir -p result
docker run -it --rm -e stage=generate -v $(pwd)/result:/root/result vbramselaar/icecat-builder
```
Then to start building GNU Icecat:
``` bash
docker run -it --rm -e stage=build -v $(pwd)/result:/root/result vbramselaar/icecat-builder
```
The archive with the source code and the compiled GNU Icecat can be found in the `result` directory.

### Locally
To build it locally, you can look at the `Dockerfile` to check the
dependencies that needs to be installed. Then, after satisfying the
dependencies, you can start compiling by running the scripts:
``` bash
./generate_icecat.sh
./build_icecat.sh
```
