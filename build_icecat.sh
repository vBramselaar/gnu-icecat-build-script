#!/bin/bash

readonly START_DIR=$(pwd)
readonly FFVERSION="115.16.0"

function extract_source ()
{
	set -e
	cd "./result"
	tar -xf "icecat-${FFVERSION}.tar.bz2"
	set +e
}

function patch_source ()
{
	set -e
	echo "ac_add_options --without-wasm-sandboxed-libraries" > "./icecat-${FFVERSION}/mozconfig"
	set +e
}

function build_source ()
{
	set -e
	cd "icecat-${FFVERSION}"
	./mach build
	./mach package
	cd ..

	mv "./icecat-${FFVERSION}/obj-x86_64-pc-linux-gnu/dist/icecat/" ./
	mkdir -p build
	tar -cvjSf build/icecat-${FFVERSION}.en-US.gnulinux-x86_64.tar.bz2 icecat
	set +e
}

extract_source
patch_source
build_source
