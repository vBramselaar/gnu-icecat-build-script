# syntax=docker/dockerfile:1
FROM debian:bookworm
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update \
	&& apt-get -y install apt-utils \
	&& apt-get -y install gnupg mercurial wget python3 python3-jsonschema patch rename xz-utils bzip2 curl

RUN apt-get -y install build-essential pkg-config libgtk2.0-dev libgtk-3-dev \
	libgconf2-dev libdbus-glib-1-dev libpulse-dev yasm python3-dev python3-pip python3-setuptools \
	python3-distutils clang nodejs nasm zip autoconf2.13 libxt-dev unzip llvm ccache libx11-xcb-dev libasound2-dev

RUN mkdir "$HOME/.mozbuild"
WORKDIR /root

RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y --default-toolchain none
ENV PATH /root/.cargo/bin:$PATH
RUN rustup default 1.66.1
RUN cargo install --force --locked cbindgen --vers 0.24.5

COPY . .
CMD ["sh", "-c", "./${stage}_icecat.sh"]
